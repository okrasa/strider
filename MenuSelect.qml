import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


Rectangle {
    anchors.left: parent.left
    anchors.right: parent.right
    height: 96
    color: "transparent"

    RowLayout {
        id: row
        spacing: 32

        anchors.fill: parent

        Text {
            id: titleText

            Layout.preferredWidth: 200
            horizontalAlignment: Text.AlignRight
            text: qsTr("Name")
            font.pixelSize: 24
            color: "#ffffff"
        }

        ComboBox {
            id: combo
            Layout.fillWidth: true
            font.pixelSize: 24

            popup.font.pixelSize: 24
        }

        Item {
            Layout.fillWidth: true
        }


    }

    property alias titleText: titleText
    property alias model: combo.model
    property alias textRole: combo.textRole
    property alias currentText: combo.currentText
    property alias currentIndex: combo.currentIndex
    property var onActivated: combo.onActivated
}
