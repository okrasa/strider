import os
from shutil import copyfile

from PySide2.QtCore import QObject, Slot


class AudioEffect(QObject):
    def __init__(self):
        QObject.__init__(self)
        copyfile('/mnt/data/strider/audio/beep-22.wav', '/tmp/beep.wav')
        copyfile('/usr/bin/aplay', '/tmp/aplay')
        os.system('chmod +x /tmp/aplay')

    @Slot()
    def beep(self):
        # os.system('aplay /mnt/data/strider/audio/beep-07a.wav')
        # os.system('aplay /mnt/data/strider/audio/button-35.wav &')
        os.system('/tmp/aplay -q /tmp/beep.wav &')