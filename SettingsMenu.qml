import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


Popup {
    id: settingsMenu
//    anchors.: parent

//    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    width: parent.width - 360
    height: parent.height
    x: 360
    y: 0
    padding: 0

    background: Rectangle {
        color: "#000000"
        opacity: 0.7
    }

    Column {
        anchors.fill: parent
        spacing: 0
        topPadding: 24+72

        MenuSlider {
            id: backlightSlider
            titleText.text: "Backlight"

            from: 0
            to: 100
            stepSize: 10
            value: 50

            onPositionChanged: {
                valueText.text = value + "%"
                settings.set_backlight(value)
            }

            Component.onCompleted: {
                valueText.text = value + "%"
                settings.set_backlight(value)
            }
        }

        MenuSlider {
            id: brightnessSlider
            titleText.text: "Brightness"

            from: 0.5
            to: 1.5
            stepSize: 0.1
            value: 1.0

            onPositionChanged: {
                valueText.text = value.toFixed(1)
                settings.set_brightness(value)
            }

            Component.onCompleted: {
                valueText.text = value.toFixed(1)
                settings.set_brightness(value)
            }
        }

        MenuSwitch {
            id: lotsSwitch
            titleText.text: "Lots"
            height: 96
        }

        MenuSwitch {
            id: debugSwitch
            titleText.text: "Debug"
            height: 96
        }

//        RowLayout {
//            id: row
//            anchors.left: parent.left
//            anchors.right: parent.right
//            spacing: 2

//            Button {
//                text: "0%"
//            }
//            Button {
//                text: "25%"
//            }
//            Button {
//                text: "50%"
//            }
//            Button {
//                text: "75%"
//                highlighted: true
//            }
//            Button {
//                text: "100%"
//            }
//        }
    }

    Settings {
        id: appSettings
        property alias backlight: backlightSlider.value
        property alias brightness: brightnessSlider.value
        property alias debug: debugSwitch.checked
        property alias lots: lotsSwitch.checked
    }

    property alias debugSwitch: debugSwitch
    property alias lotsSwitch: lotsSwitch

}
