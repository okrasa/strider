import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


Popup {
    id: mapMenu
//    anchors.: parent

//    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    width: parent.width - 360
    height: parent.height
    x: 360
    y: 0
    padding: 0

    background: Rectangle {
        color: "#000000"
        opacity: 0.7
    }

    Column {
        anchors.fill: parent
        spacing: 24
        topPadding: 24+72

        MenuSelect {
            id: mapSelect
            titleText.text: "Map type"
            // model: [ "Banana", "Apple", "Coconut" ]
        }
    }

    Settings {
        id: mapSettings
        property int mapIndex: 0
    }

    property alias mapSelect: mapSelect
    property alias mapSettings: mapSettings
}

