import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


Rectangle {
    anchors.left: parent.left
    anchors.right: parent.right
    height: 96
    color: "transparent"

    RowLayout {
        id: row
        anchors.fill: parent

        Text {
            id: titleText

            Layout.preferredWidth: 200
            horizontalAlignment: Text.AlignRight
            text: qsTr("Name")
            font.pixelSize: 24
            color: "#ffffff"
        }

        Slider {
            id: slider

            Layout.fillWidth: true
        }

        Text {
            id: valueText

            Layout.preferredWidth: 200
            text: "value"
            font.pixelSize: 24
            color: "#ffffff"
        }
    }

    property alias value: slider.value
    property alias from: slider.from
    property alias to: slider.to
    property alias stepSize: slider.stepSize
    property alias titleText: titleText
    property alias valueText: valueText

    property var onPositionChanged: slider.onPositionChanged
}
