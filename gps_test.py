#!/usr/bin/env python

# sudo usermod -a -G dialout $USER

SERIAL = '/dev/ttyACM0'

# $GPRMC,145859.00,A,5209.12068,N,02043.64435,E,0.523,,040621,,,A*77

def parse_rmc(line):
    cmd, time, warning, lat, lat1, lon, lon1, spd, crs, date, _, _, cs = line

    print(f'position: {float(lat)/100}°{lat1}, {float(lon)/100}°{lon1}, speed: {float(spd)*1.85200} km/h')


with open(SERIAL, 'r') as f:
    while True:
        line = f.readline().strip()
        if not line.startswith('$GP'):
            continue
        line = line.split(',')
        try:
            {
                '$GPRMC': parse_rmc,
                # '$GPGLL': parse_gll,
            }[line[0]](line)
        except KeyError:
            pass



