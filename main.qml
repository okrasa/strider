import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtLocation 5.6
import QtPositioning 5.6
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0

Window {
    id: mainWindow
    width: 1280
    height: 800
    visible: true
    visibility: Window.FullScreen
    title: "Strider"

//    MouseArea {
//        id: mouseArea
//        anchors.fill: parent
//        enabled: false
//        cursorShape: Qt.BlankCursor
//    }

    Component.onCompleted: {
        map.center = QtPositioning.coordinate(mainSettings.lastLatitude, mainSettings.lastLongitude)
    }

    Component.onDestruction: {
        mainSettings.lastLatitude = positionSource.position.coordinate.latitude
        mainSettings.lastLongitude = positionSource.position.coordinate.longitude
    }

    Plugin {
        id: mapboxglPlugin
        name: "mapboxgl"
        parameters: [
            PluginParameter {
                name: "mapboxgl.access_token"
                value: "pk.eyJ1Ijoia2Ftb2tyIiwiYSI6ImNrcGY2aXVqeTI0aWcyb254aGNydHFtbzAifQ.JMZzYF5wR4D-QCYNKpuiFw"
            }
//            PluginParameter {
//                name: "mapboxgl.mapping.additional_style_urls"
//                // value: "mapbox://styles/kamokr/ckpf6lhyx0xwp19qic5ins3py"
//                value: "mapbox://styles/kamokr/ckphdbhca0mdo17mk5k9lq5az"
//            }
        ]
    }

    // GPS ====================================================================

//        qml: {
//            "objectName": "",
//            "latitudeValid": true,
//            "longitudeValid": true,
//            "altitudeValid": true,
//            "coordinate": {
//                "latitude": 52.15201183333333,
//                "longitude": 20.727366500000002,
//                "altitude": 105.8,
//                "isValid": true
//            },
//            "timestamp": "2021-06-05T08:31:07.000Z",
//            "speed": 0.5619444444444445,
//            "speedValid": true,
//            "horizontalAccuracy": null,
//            "verticalAccuracy": null,
//            "horizontalAccuracyValid": false,
//            "verticalAccuracyValid": false,
//            "directionValid": false,
//            "direction": null,
//            "verticalSpeedValid": false,
//            "verticalSpeed": null,
//            "magneticVariation": null,
//            "magneticVariationValid": false
//        }

    readonly property string status_INVALID: "INVALID"
    readonly property string status_COORDINATE_VALID: "COORDINATE_VALID"
    readonly property string status_DIRECTION_VALID: "DIRECTION_VALID"
    readonly property string status_TIMEOUT: "TIMEOUT"
    readonly property string status_NONE: ""

    PositionSource {
        id: positionSource

        // name: "serialnmea"
        preferredPositioningMethods: PositionSource.SatellitePositioningMethods

        active: true
        updateInterval: 1000

        nmeaSource: "/mnt/data/strider/test2.nmea"

        //property var lastCoordinate: QtPositioning.coordinate(52.142598, 20.7175048)


        property string status: status_NONE

        function setStatus(newStatus) {
            if (newStatus != status) {
                status = newStatus
                console.log("PositionSource status changed to "+status)
                mainWindow.onStatusChanged(status)
            }
        }

        onPositionChanged:  {
//            console.log(JSON.stringify(positionSource.position, null, 4))

            positionDebugText.text = JSON.stringify(positionSource.position, null, 4)

            // if (position.directionValid) {
            //     setStatus(status_DIRECTION_VALID)
            // } else if (position.coordinate.isValid) {
            //     setStatus(status_COORDINATE_VALID)
            // } else {
            //     setStatus(status_INVALID)
            // }

            // if (status == status_COORDINATE_VALID || status == status_DIRECTION_VALID) {
            //     mainWindow.onPositionChanged(position, status)
            // }

            if (position.coordinate.isValid) {
                mainWindow.onPositionChanged(position, status)
            }
            else {
                setStatus(status_INVALID)
            }

        }

        onUpdateTimeout: {
            setStatus(status_TIMEOUT)
        }
    }

    function onStatusChanged(status) {
        if (status == status_DIRECTION_VALID) {
            marker.setMode("direction")
            buttonResetAll.icon.source = Qt.resolvedUrl("images/gps_fixed_white_48dp.svg")
        } else if (status == status_COORDINATE_VALID) {
            marker.setMode("fixed")
            buttonResetAll.icon.source = Qt.resolvedUrl("images/gps_fixed_white_48dp.svg")
        } else {
            marker.setMode("")
            buttonResetAll.icon.source = Qt.resolvedUrl("images/gps_not_fixed_white_48dp.svg")
        }
    }
    
    function onPositionChanged(position, status) {
        if(map.trackingLocked) {
            map.center = position.coordinate.atDistanceAndAzimuth(
                        map.trackingOffsetDistance,
                        map.trackingOffsetAzimuth)
        }

        if (status == status_DIRECTION_VALID) {
            // marker.rotation = position.direction - map.bearing
        }

        debug_writer.write_position(position.coordinate.latitude, position.coordinate.longitude)
        
        marker.coordinate = position.coordinate
        
        debugCircle1.updateCoordinate(position.coordinate)
        debugCircle2.updateCoordinate(position.coordinate)

        // overlay.updateCoordinate(position.coordinate)
        // overlay.coordinate = position.coordinate
        overlay.update()
    }

    // SETTINGS ===============================================================
    Settings {
        id: mainSettings
        property real lastLatitude: 0.0
        property real lastLongitude: 0.0
    }

    // MAP ====================================================================
    Map {
        id: map
        anchors.fill: parent
        plugin: mapboxglPlugin
        center: QtPositioning.coordinate(0.0, 0.0)
        zoomLevel: 17

        gesture.enabled: false

        property bool trackingLocked: true
        property real trackingOffsetAzimuth: 0
        property real trackingOffsetDistance: 0

        property var enabledMapTypes: [5, 10, 11]
        property int activeMapIndex: 0

        activeMapType: supportedMapTypes[enabledMapTypes[activeMapIndex]]

        Component.onCompleted: {
//            console.log(JSON.stringify(supportedMapTypes, null, 4))
            marker.coordinate = map.center
        }

        onBearingChanged: {
            console.debug("bearing: "+bearing)
            buttonResetRotation.rotation = -bearing
            overlay.rotation = -bearing + 1.2
        }

        onTiltChanged: {
            console.debug("tilt: "+tilt)
        }

        onZoomLevelChanged: {
            console.debug("zoomLevel: "+zoomLevel)
            overlay.update()
        }

        // TRACK ==============================================================
        property int maxTrackLength: 300

        MapPolyline {
            id: debugTrack1
            line.width: 12
            line.color: '#4040FF'
            opacity: 0.5

            path: placemarks.test()

        }

        DebugTrack {
            id: debugTrack2
            line.color: '#ff0000'

            // function onPositionChanged(position) {
                // addDebugCoordinate(position.coordinate)
                // meanTrack1.addDebugCoordinate(calculateRms(path.slice(-10)))
                // meanTrack2.addDebugCoordinate(calculateRms(path.slice(-20)))
                // meanTrack3.addDebugCoordinate(calculateRms(path.slice(-120)))
                // debugCircle25.center = calculateRms(path.slice(-30))

            // }
        }

        DebugTrack {
            id: meanTrack1
            line.color: '#ffffff'
        }

        DebugTrack {
            id: meanTrack2
            line.color: '#dddddd'
        }

        DebugTrack {
            id: meanTrack3
            line.color: '#bbbbbb'
        }

        MapCircle {
            id: debugCircle1
            radius: 10.0
            color: '#aaaaaa'
            border.width: 3
            border.color: "#ffffff"
            opacity: 0.3

            property int stopped_s: 0

            function updateCoordinate(coordinate) {

                if((isNaN(center.latitude)) || (isNaN(center.latitude))) {
                    center = coordinate
                    positionSource.setStatus(status_COORDINATE_VALID)
                }
                else if (center.distanceTo(coordinate) >= 10.0 ) {
                    marker.rotation = center.azimuthTo(coordinate) - map.bearing
                    center = coordinate
                    stopped_s = 0
                    positionSource.setStatus(status_DIRECTION_VALID)
                } 
                else {
                    stopped_s += 1
                    if(stopped_s > 5) {
                        positionSource.setStatus(status_COORDINATE_VALID)
                    }
                }
            }
        }

        MapCircle {
            id: debugCircle2
            radius: 25.0
            color: '#ff0000'
            border.width: 3
            border.color: "#ff8080"
            opacity: 0.3

            function updateCoordinate(coordinate) {
                if((isNaN(center.latitude)) || (isNaN(center.latitude))) {
                    center = coordinate
                    debugTrack2.addDebugCoordinate(coordinate)
                }
                if (center.distanceTo(coordinate) >= 25.0 ) {
                    center = coordinate
                    debugTrack2.addDebugCoordinate(coordinate)
                }
            }
        }

        // MapCircle {
        //     id: debugCircle50
        //     radius: 50.0
        //     color: '#888888'
        //     border.width: 3
        //     border.color: "#ffffff"
        //     opacity: 0.3
        // }
        MapQuickItem {
            id: overlay
            anchorPoint: Qt.point(sourceItem.width/2, sourceItem.height/2)
            rotation: 1.2
            width: 2048
            height: 2048

            property real sourceZoom: 0

            sourceItem: Image {
                id: overlayImage
                // anchors.centerIn: parent
                width: overlay.width
                height: overlay.height
               
                // source: "https://integracja.gugik.gov.pl/cgi-bin/KrajowaIntegracjaEwidencjiGruntow/wss/service/pub/guest/G2_GO_WMS/MapServer/WMSServer?VERSION=1.1.1&SERVICE=WMS&REQUEST=GetMap&LAYERS=dzialki,numery_dzialek,budynki&SRS=EPSG:2180&WIDTH=1570&HEIGHT=916&TRANSPARENT=TRUE&FORMAT=image/png&BBOX=659944,486467,660557,486824"
                // source: "images/test.png"
            }

            function update() {
                if(!overlay.visible) {
                    return
                }

                var maxDistance = 400 * Math.pow(2, 16-map.zoomLevel)

                // overlay.coordinate = map.center
                if(Math.abs(map.zoomLevel - sourceZoom) >= 1.0 ) {
                    overlay.width = 2048
                    overlay.height = 2048
                    overlay.coordinate = map.center
                    sourceZoom = map.zoomLevel
                    overlayImage.source = Qt.resolvedUrl(overlay_url.get(map.center.latitude, map.center.longitude, map.zoomLevel))
                } else if (overlay.coordinate.distanceTo(map.center) >= maxDistance) {
                    overlay.coordinate = map.center
                    overlayImage.source = Qt.resolvedUrl(overlay_url.get(map.center.latitude, map.center.longitude, sourceZoom))
                } else {
                    var c = Math.pow(2, map.zoomLevel - sourceZoom)
                    overlay.width = 2048 * c
                    overlay.height = 2048 * c
                }
                // var url = overlay_url.get(coordinate.latitude, coordinate.longitude)
                // console.log(url)
            }
        }

        // POSITION MARKER ====================================================
        MapQuickItem {
            id: marker
    //        anchorPoint.x: markerImage.width/2
    //        anchorPoint.y: markerImage.height

            // opacity: 0.7

//            sourceItem: Rectangle {
//                width: 24
//                height: 24
//                color: "transparent"
//                border.width: 4
//                border.color: "#ffffff"
//                smooth: true
//                radius: 16
//            }

//             coordinate: QtPositioning.coordinate(52.142598, 20.7175048)
            //        anchorPoint.x: markerImage.width/2
            //        anchorPoint.y: markerImage.height
            anchorPoint: Qt.point(sourceItem.width/2, sourceItem.height/2)

            sourceItem: Button {
                width: 60
                height: 60
                opacity: 0.7

                icon.source: "images/add_white_48dp.svg"
                icon.color: "#ffffff"
                icon.width: 120
                icon.height: 120

                background: Rectangle {
                    color: "#000000"
                    opacity: 0.7
                    radius: 36
                }
            }

            // sourceItem: Rectangle {
            //     width: childrenRect.width+12
            //     height: childrenRect.height+12
            //     color: "#000000"
            //     opacity: 0.7
            //     smooth: true
            //     radius: 30
                
            //     Image {
            //         id: markerImage
            //         width: 48
            //         height: 48
            //         anchors.centerIn: parent
            //         source: "images/gps_fixed_white_48dp.svg"
            //     }
            // }

            function setMode(mode) {
                if(mode == "fixed") {
                    marker.visible = true
                    marker.rotation = 0
                    sourceItem.icon.source = Qt.resolvedUrl("images/gps_fixed_white_48dp.svg")
                } else if(mode == "direction") {
                    marker.visible = true
                    sourceItem.icon.source = Qt.resolvedUrl("images/navigation_white_48dp.filled.svg")
                } else {
                    marker.visible = false
                }
            }

            // sourceItem: Image {
            //     id: markerImage
            //     width: 48
            //     height: 48
            //     source: "images/gps_fixed_white_48dp.svg"
            //     layer.enabled: true
            //     layer.effect: Rectangle {
            //         width: markerImage.width
            //         height: markerImage.height
            //         color: "#000000"
            //         opacity: 0.5
            //     }
            // }

                //         // layer.enabled: true
    //         // layer.effect: DropShadow {
    //         //     anchors.fill: messageText
    //         //     horizontalOffset: 0
    //         //     verticalOffset: 0
    //         //     radius: 12.0
    //         //     samples: 8
    //         //     color: "#ff000000"
    //         //     source: messageText
    //         // }

            // Rectangle {
            //     color: "#000000"
            //     opacity: 0.7
            //     // border.color: "#ffffff"
            //     // border.width: 8
            //     width: markerImage.width
            //     height: markerImage.height
            //     radius: 24
            // }
        }
    }

    // MAIN MENU ==============================================================
    MainMenu {
        id: mainMenu

        mapButton.onPressed: {
            mapMenu.open()
        }

        settingsButton.onPressed: {
            settingsMenu.open()
        }

        exitButton.onPressed: {
            mainWindow.close()
        }
    }

    MapMenu {
        id: mapMenu

        Component.onCompleted: {
            var mapNames = Object.keys(map.supportedMapTypes).map((key, index) => map.supportedMapTypes[key]["description"])
//            var mapNames = Object.keys(map.supportedMapTypes).map(
//                        function(key, index) {
//                            return {"id": index, "description": map.supportedMapTypes[key]["description"]}
//                        })

//            mapSelect.textRole = "description"
            mapSelect.model = mapNames
//            console.log(JSON.stringify(mapNames, null, 4))

            mapSelect.currentIndex = mapSettings.mapIndex
        }

        Component.onDestruction: {
            mapSettings.mapIndex = mapSelect.currentIndex
        }

        mapSelect.onActivated: {
            console.log(mapSelect.currentText)
            var mapIndex = mapSelect.model.indexOf(mapSelect.currentText)
            map.activeMapType = map.supportedMapTypes[mapIndex]
        }
    }

    SettingsMenu {
        id: settingsMenu

        Component.onCompleted: {
            debugGps.visible = debugSwitch.checked
            debugCircle1.visible = debugSwitch.checked
            debugCircle2.visible = debugSwitch.checked
            debugTrack1.visible = debugSwitch.checked
            overlay.visible = lotsSwitch.checked
        }

        debugSwitch.onToggled: {
            console.log("debugSwitch.onToggled")
            debugGps.visible = debugSwitch.checked
            debugCircle1.visible = debugSwitch.checked
            debugCircle2.visible = debugSwitch.checked
            debugTrack1.visible = debugSwitch.checked
            overlay.visible = lotsSwitch.checked
        }
    }

    // TOP LEFT BUTTONS
    StriderRoundButton {
        id: buttonMenu
        anchors.left: parent.left
        anchors.leftMargin: 24
        y: 24

        icon.source: "images/menu_white_48dp.svg"

        onPressed: mainMenu.open()
    }

    // BOTTOM LEFT BUTTONS
//    StriderRoundButton {
//        id: buttonFullscreen
//        anchors.left: parent.left
//        anchors.leftMargin: 24
//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: 24

//        icon.source: "images/fullscreen_white_48dp.svg"

//        onPressed: {
//            if(mainWindow.visibility == Window.Maximized) {
//                mainWindow.visibility = "FullScreen"
//            }
//            else if(mainWindow.visibility == Window.FullScreen) {
//                mainWindow.showMaximized()
//            } else {
//                mainWindow.showMaximized()
//            }
//        }
//    }

//    StriderRoundButton {
//        id: buttonChangeMap
//        anchors.left: parent.left
//        anchors.leftMargin: 24
//        anchors.bottom: buttonFullscreen.top
//        anchors.bottomMargin: 24

//        icon.source: "images/layers_white_48dp.svg"

//        onPressed: {
//            map.activeMapIndex += 1
//            map.activeMapIndex %= map.enabledMapTypes.length
//            map.activeMapType = map.supportedMapTypes[map.enabledMapTypes[map.activeMapIndex]]
//            console.log(JSON.stringify(map.activeMapType, null, 4))
//        }
//    }

    // RIGHT TOP BUTTONS ======================================================
    StriderRoundButton {
        id: buttonZoomIn
        anchors.right: parent.right
        anchors.rightMargin: 24
        y: 24

        icon.source: "images/add_white_48dp.svg"

        onPressed: {
            map.zoomLevel = Math.round( map.zoomLevel + 1 )
            message.setText("ZOOM LEVEL "+map.zoomLevel)
        }

    }

    StriderRoundButton {
        id: buttonZoomOut
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.top: buttonZoomIn.bottom
        anchors.topMargin: 24

        icon.source: "images/remove_white_48dp.svg"

        onPressed: {
            map.zoomLevel = Math.round( map.zoomLevel - 1 )
            message.setText("ZOOM LEVEL "+map.zoomLevel)
        }
    }

    // RIGHT CENTER BUTTONS ===================================================
    StriderRoundButton {
        id: buttonResetRotation
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.bottom: buttonResetTilt.top
        anchors.bottomMargin: 24

        icon.source: "images/north_white_48dp.svg"

        onPressed: {
            map.bearing = 0
            message.setText("BEARING RESET")
        }
    }

    StriderRoundButton {
        id: buttonResetTilt
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: -height/2

        icon.source: "images/window_white_48dp.svg"

        onPressed: {
            map.tilt = 0
            message.setText("TILT RESET")
        }
    }

    StriderRoundButton {
        id: buttonResetOffset
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.top: buttonResetTilt.bottom
        anchors.topMargin: 24

        icon.source: "images/filter_center_focus_white_48dp.svg"

        onPressed: {
            map.trackingOffsetDistance = 0
            map.trackingOffsetAzimuth = 0
            map.center = positionSource.position.coordinate
            message.setText("CENTERED")
        }
    }

    // RIGHT BOTTOM BUTTONS ===================================================
    StriderRoundButton {
        id: buttonLockTracking
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 24

        function update()
        {
            if(map.trackingLocked) {
                icon.source = Qt.resolvedUrl("images/lock_white_48dp.svg")
            } else {
                icon.source = Qt.resolvedUrl("images/lock_open_white_48dp.svg")
            }
        }

        Component.onCompleted: update()

        onPressed: {
            map.trackingLocked = !map.trackingLocked
            if(map.trackingLocked) {
                map.trackingOffsetDistance = positionSource.position.coordinate.distanceTo(map.center)
                map.trackingOffsetAzimuth = positionSource.position.coordinate.azimuthTo(map.center)
                map.gesture.enabled = false
                message.setText("TRACKING ENABLED")
            }
            else {
                map.gesture.enabled = true
                map.gesture.acceptedGestures = MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.RotationGesture | MapGestureArea.TiltGesture
                message.setText("TRACKING DISABLED")
            }

            update()
        }
    }

    StriderRoundButton {
        id: buttonResetAll
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.bottom: buttonLockTracking.top
        anchors.bottomMargin: 24

        icon.source: "images/gps_not_fixed_white_48dp.svg"
        property bool coordinateValid: false

        onPressed: {
            map.center = positionSource.position.coordinate
            map.trackingLocked = true
            map.gesture.enabled = false
            map.trackingOffsetDistance = 0
            map.trackingOffsetAzimuth = 0
            map.tilt = 0
            map.bearing = 0
            buttonLockTracking.update()

            message.setText("RESET")
        }
    }

    Button {
        id: message

        anchors.top: parent.top
        anchors.topMargin: 24
        anchors.horizontalCenter: parent.horizontalCenter

        height: 72
        opacity: 0.0
        leftPadding: 36
        rightPadding: 36

        background: Rectangle {
            color: "#000000"
            opacity: 0.7
            radius: 36
        }

        contentItem: Text {
            // text: control.text
            // font: control.font
            font.pixelSize: 32
            color: "#ffffff"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        NumberAnimation on opacity {
            id: messageAnimation
            from: 0.7
            to: 0.0
            duration: 2000
            easing.type: Easing.InExpo
        }

        function setText(value) {
            contentItem.text = value
            messageAnimation.restart()
        }
    }


    // Rectangle {
    //     id: message
    //     color: "#000000"

    //     anchors.top: parent.top
    //     anchors.topMargin: 40
    //     anchors.horizontalCenter: parent.horizontalCenter

    //     width: childrenRect.width+24
    //     height: childrenRect.height

    //     Text {
    //         id: messageText
    //         text: qsTr("STRIDER")
    //         font.pixelSize: 32
    //         color: "white"
    //         height: 72
    //         horizontalAlignment: Text.AlignHCenter
    //         verticalAlignment: Text.AlignVCenter

    //         // layer.enabled: true
    //         // layer.effect: DropShadow {
    //         //     anchors.fill: messageText
    //         //     horizontalOffset: 0
    //         //     verticalOffset: 0
    //         //     radius: 12.0
    //         //     samples: 8
    //         //     color: "#ff000000"
    //         //     source: messageText
    //         // }
    //     }

    //     NumberAnimation on opacity {
    //         id: messageAnimation
    //         from: 0.5
    //         to: 0.0
    //         duration: 2000
    //         easing.type: Easing.InExpo
    //     }

    //     function setText(value) {
    //         messageText.text = value
    //         messageAnimation.restart()
    //     }
    // }

    Rectangle {
        id: debugGps
        color: "#000000"
        opacity: 0.5
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 24
        Text {
            id: positionDebugText
            text: ""
            font.family: "Liberation Mono"
            font.pointSize: 8
            color: "white"
        }
        width: childrenRect.width
        height: childrenRect.height
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/
