import QtQuick.Controls 2.2
import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.12

Button {
    id: buttonId
    width: 72
    height: 72
    opacity: down ? 1.0 : 0.7

    icon.source: "images/add_white_48dp.svg"
    icon.color: "#ffffff"
    icon.width: 48
    icon.height: 48

    background: Rectangle {
        color: "#000000"
        opacity: 0.7
        // border.color: "#ffffff"
        // border.width: 8
        radius: 36
    }

    SoundEffect {
        id: playSound
        source: "audio/button-35.wav"
    }

    MouseArea {
        id: playArea
        anchors.fill: parent
        propagateComposedEvents: true
        onPressed: {
            // playSound.play()
            audioeffect.beep()
            mouse.accepted = false
        }
    }

    // layer.enabled: true
    // layer.effect: DropShadow {
    //     anchors.fill: buttonId
    //     horizontalOffset: 0
    //     verticalOffset: 0
    //     radius: 6.0
    //     samples: 8
    //     color: "#ff000000"
    //     source: buttonId
    // }
}


