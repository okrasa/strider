import QtQuick 2.0
import QtLocation 5.6
import QtPositioning 5.6

MapPolyline {
    id: debugTrack
    line.width: 3
    line.color: '#ffffff'
    path: []

    property int maxPathLength: 300

    function calculateRms(arr) {
        var lonSquares = arr.map((coord) => (coord.longitude * coord.longitude))
        var lonSum = lonSquares.reduce((acum, val) => (acum + val))
        var lonMean = lonSum/arr.length

        var latSquares = arr.map((coord) => (coord.latitude * coord.latitude))
        var latSum = latSquares.reduce((acum, val) => (acum + val))
        var latMean = latSum/arr.length

        return QtPositioning.coordinate(Math.sqrt(latMean), Math.sqrt(lonMean))
    }

    function addDebugCoordinate(coordinate) {
        debugTrack.addCoordinate(coordinate)
        var path = debugTrack.path
        path = path.slice(-maxPathLength)
        debugTrack.path = path
    }
}
