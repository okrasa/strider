import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


RowLayout {
    id: row
    spacing: 32
    anchors.left: parent.left
    anchors.right: parent.right

    Text {
        id: titleText

        Layout.preferredWidth: 200
        horizontalAlignment: Text.AlignRight
        text: qsTr("Name")
        font.pixelSize: 24
        color: "#ffffff"
    }

    Switch {
        id: sw
    }

    Item {
        Layout.fillWidth: true
    }

    property alias titleText: titleText
    property alias checked: sw.checked

    property var onToggled: sw.onToggled
}
