#!/usr/bin/env bash
export DISPLAY=:0
export QT_NMEA_SERIAL_PORT=/dev/ttyACM0
xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1
source /home/kamil/miniconda3/etc/profile.d/conda.sh
conda activate pyside
python /mnt/data/strider/main.py
