import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.0


Drawer {
    id: mainMenu
    width: 360
    height: parent.height

    background: Rectangle {
        color: "#000000"
        opacity: 1.0
    }

    Column {
        id: column
        spacing: 0
        width: parent.width
        topPadding: 24

        StriderRoundButton {
            id: backButton
            anchors.left: parent.left
            anchors.leftMargin: 24

            icon.source: "images/arrow_back_white_48dp.svg"

            onPressed: mainMenu.close()
        }

        MainMenuButton {
            id: mapButton
            text: "Map"
            background: Rectangle {
                color: "transparent"
            }
        }

        MainMenuButton {
            id: settingsButton
            text: "Settings"
        }

        MainMenuButton {
            id: exitButton
            text: "Exit"
        }
    }
    property alias mapButton: mapButton
    property alias settingsButton: settingsButton
    property alias exitButton: exitButton
}
