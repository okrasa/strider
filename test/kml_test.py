#!/usr/bin/env python
from lxml import etree as et


KML_FILE = '/mnt/data/strider/test_route1.kml'

tree = et.parse(KML_FILE)
root = tree.getroot()


# print(root.xpath('.//xmlns:kml/xmlns:Document', namespaces=root.nsmap))
# print(root.xpath('//kml/Document/*', namespaces=root.nsmap))
# print(root.nsmap)
# print(root.findall('Document', root.nsmap))


def parse_placemark(element):
    result = {}
    
    name = element.find('name', element.nsmap).text
    linestring = element.find('LineString', element.nsmap)
    point = element.find('Point', element.nsmap)
    point = None
    if linestring is not None:
        coordinates = linestring.find('coordinates', element.nsmap).text
        result[name] = [{
            'latitude': latitude,
            'longitude': longitude
        } for latitude, longitude, _ in [c.strip().split(',') for c in coordinates.split('\n') if len(c.strip()) > 0]]
        # result[name] = [c.strip().split(',') for c in coordinates.split('\n') if len(c.strip()) > 0]
    elif point is not None:
        coordinates = point.find('coordinates', element.nsmap).text
        longitude, latitude, _ = coordinates.strip().split(',')
        result[name] = [{
            'latitude': latitude,
            'longitude': longitude
        }]

    return result


    # for child in element:
    #     tag = et.QName(child)
    #     if tag.localname == "name":
    #         name = child.text
    #     if tag.localname == "LineString":
    #         coordinates = child.findall('coordinates', child.nsmap)
    #         result[child.text] = []
        

def get_placemarks(element, result={}, level=0):
    tag = et.QName(element)
    # print(' '*2*level + tag.localname + ": " + str(element.text).strip())

    # if tag.localname == "LineString":
    #     for child in element:
    #         if et.QName(child).localname == "coordinates":
    #             # print(child.text.strip())
    #             # return
    #             pass

    if tag.localname == "Placemark":
        result.update(parse_placemark(element))

    for child in element:
        result.update(get_placemarks(child, result, level+1))

    return result


placemarks = get_placemarks(root)
print(placemarks)

