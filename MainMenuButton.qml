import QtQuick 2.0
import QtQuick.Controls 2.12

Button {
    id: button
    width: parent.width
    flat: true
    font.pixelSize: 24
    height: 96

    background: Rectangle {
//                color: exitButton.down ? "#202020" : "#000000"
        color: "#000000"
    }

    contentItem: Text {
        text: button.text
        font: button.font
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        opacity: button.down ? 1.0 : 0.7
        color: "#ffffff"
    }

    MouseArea {
        id: playArea
        anchors.fill: parent
        propagateComposedEvents: true
        onPressed: {
            // playSound.play()
            audioeffect.beep()
            mouse.accepted = false
        }
    }
}
