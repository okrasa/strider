#!/usr/bin/env python
import logging

# sudo nano /etc/X11/xorg.conf.d/20-intel.conf
# Section "Device"
#    Identifier  "Intel Graphics"
#    Driver      "intel"
#    Option      "DRI"   "3"
#    Option      "TripleBuffer" "true"
#    Option      "TearFree"     "true"
# EndSection

# (pyside) kamil@tablet:/mnt/data$ cat ~/rotate_touch.sh 
# #!/usr/bin/env sh
# xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1

# export QT_NMEA_SERIAL_PORT=/dev/ttyACM0

import os
from pathlib import Path
import subprocess
import sys

#from PyQt5.QtCore import QAbstractListModel, Qt, pyqtSlot

from PySide2.QtCore import Qt, QObject, Slot, Property, Signal
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine


import placemarks as pl
from convert import wgs84_do_puwg92
from beep import AudioEffect

#class PositionModel(QAbstractItemModel):
#    LatitudeRole = Qt.UserRole + 1
#    LongitudeRole = Qt.UserRole + 2

#    _roles = {LatitudeRole: b"latitude", LongitudeRole: b"longitude"}

#    def __init__(self):
#        QAbstractItemModel.__init__(self)


class Settings(QObject):
    @Slot(str)
    def set_backlight(self, v):
        v = str(int(v))
        logging.info(f'SETTINGS backlight {v}%')
        subprocess.run(['xbacklight', '-set', v, '-steps', '1'])

    @Slot(str)
    def set_brightness(self, v):
        logging.info(f'SETTINGS brightness {float(v):.1f}')
        subprocess.run(['xrandr', '--output', 'DSI1', '--brightness', v])


class DebugWriter(QObject):
    def __init__(self):
        QObject.__init__(self)

    @Slot(float, float)
    def write_position(self, latitude, longitude):
        x, y = wgs84_do_puwg92(latitude, longitude)
        # logging.debug(f'lat: {latitude}, lon: {longitude}, x: {x}, y: {y}')
        # print(f'lat: {latitude}, lon: {longitude}, x: {x}, y: {y}')


class OverlayUrl(QObject):
    def __init__(self):
        QObject.__init__(self)

    @Slot(float, float, float, result=str)
    def get(self, latitude, longitude, zoom_level):
        x, y = wgs84_do_puwg92(latitude, longitude)

        # zoom = {
        #     14: 6000,
        #     15: 3000,
        #     16: 1500,
        #     17: 750,
        #     18: 375,
        #     19: 375/2,
        #     20: 375/4,
        #     21: 375/8
        # }

        # if(zoom_level//1 == zoom_level):
        scale = 6000 / pow(2, zoom_level-14)

        # scale = zoom[zoom_level]
        url = f'https://integracja.gugik.gov.pl/cgi-bin/KrajowaIntegracjaEwidencjiGruntow/wss/service/pub/guest/G2_GO_WMS/MapServer/WMSServer?VERSION=1.1.1&SERVICE=WMS&REQUEST=GetMap&LAYERS=dzialki,numery_dzialek,budynki&SRS=EPSG:2180&WIDTH=2048&HEIGHT=2048&TRANSPARENT=TRUE&FORMAT=image/png&BBOX={y-scale+1},{x-scale+3},{y+scale+1},{x+scale+3}'
        # logging.debug(url)
        print(url)
        return url

# class Placemarks(QObject):
#     value_changed = Signal(list)
#     test_changed = Signal(str)

#     def __init__(self):
#         QObject.__init__(self)

#     def _value(self):
#         return [
#             {'latitude': '20.75276', 'longitude': '51.883'},
#             {'latitude': '20.75231', 'longitude': '51.88294'},
#             {'latitude': '20.75129', 'longitude': '51.88274'},
#             {'latitude': '20.75035', 'longitude': '51.88252'}
#         ]

#     def _test(self):
#         return 'test'

#     pp = Property(list, _value, notify=value_changed)
#     test = Property(str, _test, notify=test_changed)


class Placemarks(QObject):
    def __init__(self):
        QObject.__init__(self)

    @Slot(result='QVariant')
    def test(self):
        p = pl.load()
        return p[list(p.keys())[0]]
        # return [
        #     {'longitude': '20.75276', 'latitude': '51.883'},
        #     {'longitude': '21.75231', 'latitude': '52.88294'},
        #     {'longitude': '22.75129', 'latitude': '53.88274'},
        #     {'longitude': '23.75035', 'latitude': '54.88252'}
        # ]


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    settings = Settings()
    audioeffect = AudioEffect()
    debug_writer = DebugWriter()
    placemarks = Placemarks()
    overlay_url = OverlayUrl()

    QGuiApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    app = QGuiApplication(sys.argv)
    app.setOrganizationName("LANTEQ Kamil Okrasa")
    app.setOrganizationDomain("lanteq.pl")
    app.setOverrideCursor(Qt.BlankCursor)

    engine = QQmlApplicationEngine()
    engine.rootContext().setContextProperty("settings", settings)
    engine.rootContext().setContextProperty("audioeffect", audioeffect)
    engine.rootContext().setContextProperty("debug_writer", debug_writer)
    engine.rootContext().setContextProperty("placemarks", placemarks)
    engine.rootContext().setContextProperty("overlay_url", overlay_url)

    engine.load(os.fspath(Path(__file__).resolve().parent / "main.qml"))
    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())
