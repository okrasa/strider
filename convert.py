from math import pi, sin, cos, tan, atan, log


M_PI = pi


def wgs84_do_puwg92(B_stopnie, L_stopnie):
    # parametry elipsoidy GRS-80
    e = 0.0818191910428   	# pierwszy mimośród elipsoidy
    R0 = 6367449.14577  	# promieñ sfery Lagrange'a
    Snorm = 2.0E-6    		# parametr normujący
    xo = 5760000.0  		# parametr centrujący
    
    # wspolczynniki wielomianu
    a0 = 5765181.11148097 
    a1 = 499800.81713800 
    a2 = -63.81145283 
    a3 = 0.83537915 
    a4 = 0.13046891 
    a5 = -0.00111138 
    a6 = -0.00010504 
    
    # parametry odwzorowania Gaussa-Kruegera dla układu PUWG92
    L0_stopnie = 19.0  		# początek układu wsp. PUWG92 (długość)
    m0 = 0.9993 
    x0 = -5300000.0 
    y0 =  500000.0 
    
    # zakres stosowalnosci metody
    Bmin = 48.0 * M_PI / 180.0 
    Bmax = 56.0 * M_PI / 180.0 
    dLmin = -6.0 * M_PI / 180.0 
    dLmax = 6.0 * M_PI / 180.0 
    
    # weryfikacja danych wejsciowych
    B = B_stopnie * M_PI / 180.0 
    dL_stopnie = L_stopnie - L0_stopnie 
    dL = dL_stopnie * M_PI / 180.0 

    if ((B < Bmin) or (B > Bmax)):
          return 1
          
    if ((dL < dLmin) or (dL > dLmax)):
          return 2

    # etap I - elipsoida na kulę
    U = 1.0 - e * sin(B)
    V = 1.0 + e * sin(B)
    K = pow((U / V), (e / 2.0))
    C = K * tan(B / 2.0 + M_PI / 4.0)
    fi = 2.0 * atan(C) - M_PI / 2.0
    d_lambda = dL

    # etap II - kula na walec
    p = sin(fi)
    q = cos(fi) * cos(d_lambda)
    r = 1.0 + cos(fi) * sin(d_lambda)
    s = 1.0 - cos(fi) * sin(d_lambda)
    XMERC = R0 * atan(p / q)
    YMERC = 0.5 * R0 * log(r / s)

    # etap III - walec na płaszczyznę
    Z = complex((XMERC - xo) * Snorm, YMERC * Snorm)
    Zgk = a0+Z*(a1+Z*(a2+Z*(a3+Z*(a4+Z*(a5+Z*a6)))))

    Xgk = Zgk.real
    Ygk = Zgk.imag
    
    # przejście do układu aplikacyjnego
    Xpuwg = m0 * Xgk + x0
    Ypuwg = m0 * Ygk + y0
    
    return Xpuwg, Ypuwg


if __name__ == '__main__':
    lat = 52.1426900
    lon = 20.7169700
    print(wgs84_do_puwg92(lat, lon))

# int puwg92_do_wgs84(double Xpuwg, double Ypuwg, double *B_stopnie, double *L_stopnie)
# /*
# Opis:
#     konwersja wspolrzednych z ukladu PUWG 1992 do ukladu WGS 84
# Parametry:
#     Xpuwg - wskazanie na wspolrzedna X ukladu PUWG 1992 (UWAGA - wspolrzedna pionowa)
#     Ypuwg - wskazanie na wspolrzedna Y ukladu PUWG 1992 (UWAGA - wspolrzedna pozioma)
#     B_stopnie - szerokosc geograficzna wyrazona w stopniach
#     L_stopnie - dlugosc geograficzna wyrazona w stopniach
# Zwracana wartosc:
#     0 - konwersja powiodla sie
# */  
# 	{
# 	double L0_stopnie=19.0; 		//Pocz±tek uk³adu wsp. PUWG92 (d³ugo¶æ)
# 	double m0=0.9993;
#     double x0=-5300000.0;
#     double y0= 500000.0;
    
#     double R0=6367449.14577; 	//promieñ sfery Lagrange.a
#     double Snorm=2.0E-6;   		//parametr normuj±cy
#     double xo_prim=5765181.11148097; 		//parametr centruj±cy
    
#     // Wspolczynniki wielomianu
#     double b0=5760000;
#     double b1=500199.26224125;
#     double b2=63.88777449;
#     double b3=-0.82039170;
#     double b4=-0.13125817;
#     double b5=0.00101782;
#     double b6=0.00010778;
        
#     // Wspolczynniki szeregu tryg.
#     double c2=0.0033565514856;
#     double c4=0.0000065718731;
#     double c6=0.0000000176466;
#     double c8=0.0000000000540;
    
# 	//Przejscie z ukladu aplikacyjnego
# 	double Xgk, Ygk;
# 	Xgk=(Xpuwg-x0)/m0;
# 	Ygk=(Ypuwg-y0)/m0;
		
# 	//etap I - (Xgk, Ygk) -> (Xmerc, Ymerc)
# 	complex<double> Z((Xgk-xo_prim)*Snorm,Ygk*Snorm);
# 	complex<double> Zmerc;
	
# 	Zmerc=b0+Z*(b1+Z*(b2+Z*(b3+Z*(b4+Z*(b5+Z*b6)))));
	
# 	double Xmerc=Zmerc.real(); 
# 	double Ymerc=Zmerc.imag();
	
# 	//etap II - Xmerc,Ymerc -> fi, delta_lambda
# 	double alfa=Xmerc/R0;
# 	double beta=Ymerc/R0;
	
# 	double w=2.0*atan(exp(beta))-M_PI/2.0;
# 	double fi=asin(cos(w)*sin(alfa));
# 	double d_lambda=atan(tan(w)/cos(alfa));
	
# 	//etap III
# 	double B=fi+c2*sin(2.0*fi)+c4*sin(4.0*fi)+c6*sin(6.0*fi)+c8*sin(8.0*fi);
# 	double dL=d_lambda;
	
# 	//Obliczenia koncowe
# 	*B_stopnie=B/M_PI*180.0;
# 	double dL_stopnie=dL/M_PI*180.0;
# 	*L_stopnie=dL_stopnie+L0_stopnie;
    
# 	return 0;
# 	}