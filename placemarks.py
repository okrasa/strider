from lxml import etree as et

KML_FILE = '/mnt/data/strider/test_route1.kml'



def _parse_placemark(element):
    result = {}
    
    name = element.find('name', element.nsmap).text
    linestring = element.find('LineString', element.nsmap)
    point = element.find('Point', element.nsmap)
    point = None
    if linestring is not None:
        coordinates = linestring.find('coordinates', element.nsmap).text
        result[name] = [{
            'latitude': latitude,
            'longitude': longitude
        } for longitude, latitude, _ in [c.strip().split(',') for c in coordinates.split('\n') if len(c.strip()) > 0]]
    elif point is not None:
        coordinates = point.find('coordinates', element.nsmap).text
        longitude, latitude, _ = coordinates.strip().split(',')
        result[name] = [{
            'latitude': latitude,
            'longitude': longitude
        }]

    return result
        

def load_placemarks(element, result={}, level=0):
    tag = et.QName(element)

    if tag.localname == "Placemark":
        result.update(_parse_placemark(element))

    for child in element:
        result.update(load_placemarks(child, result, level+1))

    return result

def load():
    tree = et.parse(KML_FILE)
    root = tree.getroot()
    return load_placemarks(root)